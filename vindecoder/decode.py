from django.http import HttpResponse
from django.shortcuts import render
from vindecoder.validate import validate


def decode(request):
    context = {}
    try:
        if validate(request.POST['vinnumber']) is True:
            context['rslt'] = 'OK'
        else:
             context['rslt'] = 'Wrong checksum!'
    except Exception as e:
        context['rlst'] = str(e)
    return render(request, 'vindecoder.html', context)